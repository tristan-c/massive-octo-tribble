"""Import Bookmarks.

Usage:
    addUser.py (USERNAME)
"""

import datetime

from massive import user_datastore
from docopt import docopt

if __name__ == '__main__':
    arguments = docopt(__doc__)

    passwd = input("Enter password: ")

    user = user_datastore.create_user(
        email=arguments['USERNAME'],
        password=passwd,
        confirmed_at=datetime.datetime.now())
    user.save()
    print("user %s saved" % user.email)
