from flask import Flask
from flask_restful import Api
from flask_mongoengine import MongoEngine
from flask_security import Security, MongoEngineUserDatastore
from flask_mail import Mail


app = Flask(__name__, static_url_path='')
app.config.from_object('config')

api = Api(app)
db = MongoEngine(app)

mail = Mail(app)

from massive.models import User, Role
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)

app.login_manager.session_protection = 'strong'

import massive.auth
import massive.views
