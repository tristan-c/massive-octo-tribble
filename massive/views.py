import json
import urllib
import base64

from bs4 import BeautifulSoup
from flask_restful import Resource, reqparse
from flask_login import login_required
from flask import Response
from flask_security import current_user
import html2text
import requests

from massive import api
from massive.models import *

h = html2text.HTML2Text()


class Resource(Resource):
    method_decorators = [login_required]


class home(Resource):
    def get(self):
        return redirect('/index.html')


api.add_resource(home, '/home')


parser = reqparse.RequestParser()
parser.add_argument('url', type=str)
parser.add_argument('tags', type=str, default=None)


class links(Resource):
    def get(self):
        user = User.objects.get(id=current_user.id)
        data = [link.dump() for link in user.links]
        json_data = json.dumps(data, indent=4, sort_keys=True, default=str)
        return Response(
            response=json_data,
            status=200,
            mimetype='application/json')

    def post(self, linkId=None):
        args = parser.parse_args()
        user = User.objects.get(id=current_user.id)

        url = args['url']
        tags = args['tags']

        # prepend if no protocole specified
        if url.find("http://") == -1 and url.find("https://") == -1:
            url = "http://%s" % url

        try:
            if Link.objects.get(url=url, user=current_user.id).first():
                return "already in db", 400
        except Exception as e:
            pass

        tags = tags.split(",") if tags else []

        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)'
            ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47'
            ' Safari/537.36'
        }

        try:
            r = requests.get(url, headers=headers)
        except Exception as e:
            return None

        content = r.text

        # save to md format
        h = html2text.HTML2Text()
        h.protect_links = True
        h.baseurl = url
        md_text = h.handle(content)

        soup = BeautifulSoup(content, "html.parser")

        title = None
        if soup.title:
            title = soup.title.string
        icon_link = soup.find("link", rel="shortcut icon")

        icon = None
        if icon_link:
            try:
                icon = urllib.request.urlopen(
                    urllib.parse.urljoin(url, icon_link['href']))
            except Exception as e:
                pass

        link = Link()
        link.title = str(title) if title else ''
        link.markdown = str(md_text)
        if icon:
            link.favicon = base64.b64encode(icon.read())
        link.tags = tags
        link.url = url
        link.save()

        user.links.append(link)
        user.save()
        return {'success': True}

    def delete(self, linkId=None):
        if linkId is None:
            return "no link provided", 400

        link = Link.query.get_or_404(linkId)
        link.delete()
        return "deleted", 200


api.add_resource(links, '/links', '/links/<string:linkId>')
