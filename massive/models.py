from flask_security import UserMixin, RoleMixin
import mongoengine
from base64 import b64decode

from massive import db


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True, choices=[
        'owner',
        'user',
        'admin']),
    description = db.StringField(max_length=255)


class User(db.Document, UserMixin):
    email = db.EmailField(max_length=255, unique=True)
    password = db.StringField(max_length=255)
    confirmed_at = db.DateTimeField()
    active = db.BooleanField(default=False)
    roles = db.ListField(db.ReferenceField(Role), default=[])

    links = db.ListField(db.ReferenceField('Link'),
                         reverse_delete_rule=mongoengine.PULL,
                         default=[])

    def __repr__(self):
        return '<User %r>' % (self.email)


class Link(db.Document):
    user = db.ReferenceField('User')
    description = db.StringField(max_length=255)
    url = db.StringField(max_length=255)
    title = db.StringField(max_length=255)
    favicon = db.BinaryField()
    tags = db.ListField(db.StringField())
    markdown = db.StringField()

    def dump(self):
        link = {
            "_id": str(self.id),
            "url": self.url,
            "title": self.title,
            "description": self.description,
            "tags": self.tags,
        }

        if self.favicon:
            link['favicon'] = self.favicon.decode('utf-8')

        return link

    def __repr__(self):
        return '<Link %r>' % (self.url)
