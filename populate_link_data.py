import urllib
import base64

from bs4 import BeautifulSoup
import html2text
import requests

from massive.models import Link

for link in Link.objects:
    url = link.url

    if url.find("http://") == -1 and url.find("https://") == -1:
        url = "http://%s" % url

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)'
        ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47'
        ' Safari/537.36'
    }

    try:
        r = requests.get(url, headers=headers)
    except Exception as e:
        continue

    content = r.text

    # save to md format
    h = html2text.HTML2Text()
    h.protect_links = True
    h.baseurl = url
    md_text = h.handle(content)

    soup = BeautifulSoup(content, "html.parser")

    title = None
    if soup.title:
        title = soup.title.string
    icon_link = soup.find("link", rel="shortcut icon")

    icon = None
    if icon_link:
        try:
            icon = urllib.request.urlopen(
                urllib.parse.urljoin(url, icon_link['href']))
        except Exception as e:
            pass

    link.title = str(title) if title else ''
    link.markdown = str(md_text)
    if icon:
        link.favicon = base64.b64encode(icon.read())

    link.save()
