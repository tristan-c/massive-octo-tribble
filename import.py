import json
from massive import user_datastore
from massive.models import *

u1 = user_datastore.create_user(
    email='tristan@carranante.name', password='passwordisawoo')
u2 = user_datastore.create_user(
    email='rondeau.clement@gmail.com', password='passwordisawoo')
u3 = user_datastore.create_user(
    email='ben.teissier@gmail.com', password='passwordisawoo')


with open('dump.json') as f:
    links = json.load(f)

    for link in links:

        if link['user_id'] in [1, 2, 3, 7]:
            li = Link(url=link['url'])
            li.tags = link['tags']
            if link['user_id'] == 1:
                li.user = u1
                u1.links.append(li)
            if link['user_id'] == 2:
                li.user = u2
                u2.links.append(li)
            if link['user_id'] == 3:
                li.user = u3
                u3.links.append(li)
            if link['user_id'] == 7:
                li.user = u1
                u1.links.append(li)

            li.save()

u1.save()
u2.save()
u3.save()